package com.cch.platform.base.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cch.platform.base.service.HibernateService;
import com.cch.platform.web.WebUtil;


@Controller
@RequestMapping(value = "/base/hibernate")
public class HibernateController {

	@Autowired
	HibernateService ms=null;
	
	@RequestMapping(value = "buffer.do")
	public String buffer(HttpServletRequest request,ModelMap mm) {
		try {
			ms.hibernateBuffer();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "jsonView";
	}
	
	@RequestMapping(value = "reloadConfig.do")
	public ModelAndView reloadConfig() {
		try {
			ms.reloadConfig();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return WebUtil.sucesseView("加载成功");
	}
	
}
