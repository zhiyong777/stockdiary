package com.cch.platform.base.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BaseRolemenu entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "base_rolemenu")
public class BaseRolemenu implements java.io.Serializable {

	// Fields

	private Integer id;
	private String roleCode;
	private String menuCode;

	// Constructors

	/** default constructor */
	public BaseRolemenu() {
	}

	/** full constructor */
	public BaseRolemenu(Integer id, String roleCode, String menuCode) {
		this.id = id;
		this.roleCode = roleCode;
		this.menuCode = menuCode;
	}

	// Property accessors
	@Id
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "role_code", nullable = false, length = 100)
	public String getRoleCode() {
		return this.roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	@Column(name = "menu_code", nullable = false, length = 100)
	public String getMenuCode() {
		return this.menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

}