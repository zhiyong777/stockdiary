package com.cch.platform.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

public class BaseDao<T> extends HibernateDao {
	
	protected Class<T> entityClass;
	
	@SuppressWarnings("unchecked")
	public  BaseDao(){
		try {
			Type genType = getClass().getGenericSuperclass();
			Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
			entityClass = (Class<T>) params[0];	
		} catch (Exception e) {
			entityClass=null;
		}
	}

	public T get(Serializable id) {
		return this.get(entityClass,id);
	}
	
	public T get(Map<String, Object> param) {
		return this.get(entityClass,param);
	}
	
	public Map<String,Object> pageQuery(Map<String, Object> param) {
		return this.pageQuery(entityClass,param);
	}
	
	public List<T> find(Map<String, Object> params){
		return this.find(entityClass,params);
	}

	
}
