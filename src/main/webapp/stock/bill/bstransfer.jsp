<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<jsp:include page="/core/tool/resources.jsp"></jsp:include>
	<script src="./bstransfer.js"></script>
	<script src="../../core/jquery/jquery.upload.js"></script>
</head>

<body>
	<div id="toolbar">
	   <a href="#" class="easyui-linkbutton" iconCls="icon-add" onclick="javascript:$('#dg').edatagrid('addRow')">新增</a>
       <a href="#" class="easyui-linkbutton" onclick="click_save()" iconCls='icon-save'>保存</a>
       <a href="#" class="easyui-linkbutton" iconCls="icon-remove" onclick="javascript:$('#dg').edatagrid('destroyRow')">删除</a>
       <a href="#" class="easyui-linkbutton" iconCls="icon-undo" onclick="javascript:$('#dg').edatagrid('cancelRow')">取消</a>
       <a href="#" class="easyui-linkbutton" onclick="click_upload()" id="upload">导入</a>
	</div>
	<table id="dg" title="银证转账" toolbar="#toolbar" sortOrder='desc' sortName='billDate' style="margin-top:20px"
            data-options="rownumbers:true,singleSelect:true,pagination:true,method:'post',idField:'billId',
            	pageSize:20,
            	fit:true">
        <thead>
            <tr>
           	 	<th data-options="field:'billId'" hidden='true'>主键</th>
                <th data-options="field:'billCatory',width:80,flexset:'SD_BSTRANS_TYPE',editor:'combobox'">类别</th>
                <th data-options="field:'billMoney',width:80,editor:'numberbox',align:'right'" sortable="true">转账金额</th>
                <th data-options="field:'billDate',width:140,editor:'datetimebox',order:'desc'" sortable="true">转账时间</th>
                <th data-options="field:'billDesc',width:300,editor:'text'">说明</th>
            </tr>
        </thead>
    </table>
</body>
</html>
